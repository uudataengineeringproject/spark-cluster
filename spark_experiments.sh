#!/bin/bash

echo "Strong scaling"
echo "1.0 as input parameters because we the problem size is fixed"

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 1.0 1 &> outputLogs/output1

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 1.0 2 &> outputLogs/output2

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 1.0 3 &> outputLogs/output3

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 1.0 4 &> outputLogs/output4

echo "Weak scaling"
echo "25%, 50%, 75% and 100% sample for each execution"

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 0.25 1 &> outputLogs/output_weak1

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 0.5 2 &> outputLogs/output_weak2

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 0.75 3 &> outputLogs/output_weak3

spark-submit \
    --name "Sample Spark Application Adri" \
    --master spark://hadoop-master:7077 \
    --driver-memory 3g \
    --executor-memory 2g \
    --executor-cores 1 \
    spark_job.py 1.0 4 &> outputLogs/output_weak4