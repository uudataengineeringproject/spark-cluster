from pyspark.sql import SparkSession
import time
import sys

# Input parameters
# give sample percentage as input parameters in spark-submit 25%, 50%, 70%, 100% -> [0.25, 0.5, 1.0]
sample_percentage = float(sys.argv[1])

# Max number of cores to use
max_cores = sys.argv[2]

spark = SparkSession \
    .builder \
    .config("spark.cores.max", max_cores) \
    .appName("testing spark scaling") \
    .getOrCreate()

data_2008 = spark.read.json('hdfs://hadoop-master:9000/user/ubuntu/RC/RC_2008-12.json')\
    .sample(sample_percentage).cache()
data_2012 = spark.read.json('hdfs://hadoop-master:9000/user/ubuntu/RC/RC_2012-12.json')\
    .sample(sample_percentage).cache()
data_2008.printSchema()

print("The count for 2008 is" + str(data_2008.count()))
print("The count for 2014 is" + str(data_2012.count()))
# bring data to memory before starting to measure the time
start = time.time()
print("hello")

print("The count for 2008 after filter is" + str(data_2008.filter("controversiality > 0").count()))
print("The count for 2014 after filter is" + str(data_2012.filter("controversiality > 0").count()))

end = time.time()
print("Total time: +"+str(end - start))