#!/bin/bash

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get autoremove
sudo apt install default-jdk

wget https://ftp.acc.umu.se/mirror/apache.org/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz 
wget https://ftp.acc.umu.se/mirror/apache.org/spark/spark-3.1.1/spark-3.1.1-bin-hadoop3.2.tgz

tar xzf hadoop-3.2.2.tar.gz
tar xzf spark-3.1.1-bin-hadoop3.2.tgz
sudo mv hadoop-3.2.2 /usr/local/hadoop
sudo mv spark-3.1.1-bin-hadoop3.2 /opt/spark
rm hadoop-3.2.2.tar.gz
rm spark-3.1.1-bin-hadoop3.2.tgz

echo \
'
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin
export SPARK_HOME=/opt/spark
export HADOOP_HOME=/usr/local/hadoop
export PATH=$PATH:$SPARK_HOME/sbin
export PATH=$PATH:$HADOOP_HOME/bin
export PATH=$PATH:$HADOOP_HOME/sbin
export YARN_HOME=$HADOOP_HOME
'>> ~/.bashrc

source .bashrc

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin
export SPARK_HOME=/opt/spark
export HADOOP_HOME=/usr/local/hadoop
export PATH=$PATH:$SPARK_HOME/sbin
export PATH=$PATH:$HADOOP_HOME/bin
export PATH=$PATH:$HADOOP_HOME/sbin
export YARN_HOME=$HADOOP_HOME

echo \
'
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://hadoop-master:9000</value>
    </property>
</configuration>
'>$HADOOP_HOME/etc/hadoop/core-site.xml

echo \
'
<configuration>
     <property>
            <name>dfs.replication</name>
            <value>1</value>
     </property>
     <property>
            <name>dfs.namenode.name.dir</name>
	    <value>/usr/local/hadoop/hdfs/name</value>
     </property>
     <property>
            <name>dfs.datanode.data.dir</name>
	    <value>/usr/local/hadoop/hdfs/data</value>
     </property>
     <property>
            <name>dfs.permissions</name>
            <value>false</value>
     </property>
</configuration>
'>$HADOOP_HOME/etc/hadoop/hdfs-site.xml

echo "export JAVA_HOME=${JAVA_HOME}" > $HADOOP_HOME/etc/hadoop/hadoop-env.sh

#modify /etc/hosts with the right ips and hostnames
#sudo vim /etc/hostname and change the hostname (e.g. "hadoop-master", hadoop-slave1", etc)
#sudo vim /usr/local/hadoop/etc/hadoop/workers (add hadoop-slave1, hadoop-slave2, etc)
#sudo vim /opt/spark/conf/slaves (add hadoop-slave1, hadoop-slave2, etc)
#start-dfs.sh
#start-all.sh

