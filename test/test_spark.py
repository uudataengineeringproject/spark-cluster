from pyspark import SparkSession

spark = SparkSession\
    .getOrCreate()\
    .master("spark://hadoop-master:7077")

data = spark.read.json('hdfs://hadoop-master:9000/reddit_sample.json')
data.printSchema()

data.take(1).write.format("json").save('hdfs://hadoop-master:9000/user/ubuntu/output_test')
