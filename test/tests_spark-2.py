#!/usr/bin/env python
# coding: utf-8


from pyspark.sql import SparkSession
import time
import sys




#Strong scalability
#Take all data from both years and test with 1, 2, 4, 8 executors


#strong
spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",1)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
    
# give sample percentage as input parameters in spark-submit 25%, 50%, 100% -> [0.25, 0.5, 1.0]
#sample_percentage = sys[1]
sample_percentage = 1.0

data_2006 = spark.read.json('RC_2006*').sample(sample_percentage).cache()
data_2007 = spark.read.json('RC_2007*').sample(sample_percentage).cache()

strong1core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",2)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
    
strong2core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",4)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
    
strong4core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",8)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
    
strong8core = work()



#Weak scalability
#Take all data from both years and test with 1, 2, 4, 8 executors
#Sample the data 12%, 25%, 50%, 100%


#weak
spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",1)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
    
# give sample percentage as input parameters in spark-submit 25%, 50%, 100% -> [0.25, 0.5, 1.0]
#sample_percentage = sys[1]
sample_percentage = 0.12

data_2006 = spark.read.json('RC_2006*').sample(sample_percentage).cache()
data_2007 = spark.read.json('RC_2007*').sample(sample_percentage).cache()

weak1core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",2)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\
sample_percentage = 0.25

data_2006 = spark.read.json('RC_2006*').sample(sample_percentage).cache()
data_2007 = spark.read.json('RC_2007*').sample(sample_percentage).cache()
    
weak2core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",4)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\

sample_percentage = 0.5

data_2006 = spark.read.json('RC_2006*').sample(sample_percentage).cache()
data_2007 = spark.read.json('RC_2007*').sample(sample_percentage).cache()
    
weak4core = work()

spark = SparkSession    .builder    .master("spark://spark-master:7077")    .config("spark.executor.cores",8)    .appName("testing spark")    .getOrCreate()
#     .config("spark.dynamicAllocation.enabled", True)\
#     .config("spark.shuffle.service.enabled", True)\
#     .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#.config("spark.driver.port",9998)\
#.config("spark.blockManager.port",10005)\

sample_percentage = 1

data_2006 = spark.read.json('RC_2006*').sample(sample_percentage).cache()
data_2007 = spark.read.json('RC_2007*').sample(sample_percentage).cache()
    
weak8core = work()



def work():
    data_2006.count()
    data_2007.count()
    # bring data to memory before starting to measure the time
    start = time.time()
    print("hello")
    
    print(data_2006.filter("controversiality > 0").count())
    print(data_2007.filter("controversiality > 0").count())
    
    #data.write.mode("overwrite").format("json").save('pupu')
    end = time.time()
    print(end - start)
    return(end-start)
    

