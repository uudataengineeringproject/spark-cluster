from pyspark.sql import SparkSession

# # New API
# spark_session = SparkSession\
#         .builder\
#         .master("spark://hadoop-master:7077") \
#         .config("spark.dynamicAllocation.enabled", True)\
#         .config("spark.shuffle.service.enabled", True)\
#         .config("spark.dynamicAllocation.executorIdleTimeout","30s")\
#         .config("spark.executor.cores",3)\
#         .config("spark.driver.port",9998)\
#         .config("spark.blockManager.port",10005)\
#         .appName("benblamey_read_weather_data")\
#         .getOrCreate()

spark_session = (
        SparkSession.builder
        # This doesn't seem to have an impact on YARN.
        # Use `spark-submit --name` instead.
        # .appName('Sample Spark Application')
        .getOrCreate())

spark_session.sparkContext.setLogLevel('WARN')

data_frame = spark_session.read\
    .option("header", "true")\
    .json('hdfs://hadoop-master:9000/user/ubuntu/reddit_sample')\
    .cache()

data_frame.printSchema()

data_frame.count()

data_frame.coalesce(1).write.mode("overwrite").csv('hdfs://hadoop-master:9000/user/ubuntu/output_test')